# WatchThis - Backend Auth

## Fonctionnement API Auth
### Auth - POST

Code de retour :
* 200 : OK
* 401 : Unauthorized

`/api/auth/`

Structure de données : 
````
{
	"mail":  mail,
	"pwd":  pwd
}
````

Envoie un JWT Token

### Current - GET

Necessite un JWT token
Code de retour :
* 200 : OK
* 404 : Not found
* 401 : Unauthorized

`/api/auth/current`

Renvoie un utilisateur :
Structure de données : 
````
{
	"Nom":  "nn",
	"Prenom":  "pp",
	"Mail":  "mm",
	"Pwd":  "pwd"
}
````

### Logout - GET

Necessite un JWT token
Code de retour :
* 200 : OK
* 401 : Unauthorized

`/api/auth/logout`

Supprime l'entete JWT