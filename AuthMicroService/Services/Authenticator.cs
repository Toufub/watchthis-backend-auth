﻿using AuthMicroService.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace AuthMicroService.Services
{
    public class Authenticator
    {
        private static IConfiguration _config;
        public static void init(IConfiguration config)
        {
            _config = config;
        }

        public static JwtSecurityToken GetToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            token = token.Replace("Bearer ", string.Empty);
            tokenHandler.ValidateToken(token, new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = securityKey,
                ValidateIssuer = false,
                ValidateAudience = false,
                ClockSkew = TimeSpan.Zero
            }, out SecurityToken validatedToken);
            return (JwtSecurityToken)validatedToken;
        }

        //To authenticate user
        public static Utilisateur Authenticate(LoginForm loginForm)
        {
            loginForm.Pwd = Encoding.ASCII.GetString(PasswordManager.HashPassword(loginForm.Pwd));
            HttpContent checkPassword = Connect.Post("utilisateurs/verifypassword", JsonSerializer.Serialize(loginForm));
            if(checkPassword != null)
            {
                return checkPassword.ReadFromJsonAsync<Utilisateur>().Result;
            }
            else
            {
                return null;
            }
        }
        // To generate token
        public static string GenerateToken(Utilisateur utilisateur)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, utilisateur.UtilisateurId.ToString()),
                new Claim(ClaimTypes.Email, utilisateur.Mail),
                new Claim(ClaimTypes.Name, utilisateur.Nom),
                new Claim(ClaimTypes.Surname, utilisateur.Prenom),
                new Claim(ClaimTypes.GivenName, utilisateur.Prenom + " " + utilisateur.Nom)
            };
            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
                _config["Jwt:Audience"],
                claims,
                expires: DateTime.Now.AddMinutes(1440),
                signingCredentials: credentials);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

    }
}
