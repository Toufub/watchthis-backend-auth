﻿using AuthMicroService.Models;
using System.Security.Cryptography;
using System.Text;

namespace AuthMicroService.Services
{
    public class PasswordManager
    {
        public static byte[] HashPassword(string input)
        {
            return SHA256.HashData(Encoding.ASCII.GetBytes(input));
        }

    }
}
