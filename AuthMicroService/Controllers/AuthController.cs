﻿using AuthMicroService.Models;
using AuthMicroService.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Text.Json;

namespace AuthMicroService.Controllers
{
    [Route("")]
    [ApiController, Authorize]
    public class AuthController : ControllerBase
    {
        private readonly IConfiguration _config;
        public AuthController(IConfiguration config)
        {
            _config = config;
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login([FromBody] LoginForm loginForm)
        {
            Authenticator.init(_config);
            var utilisateur = Authenticator.Authenticate(loginForm);
            if (utilisateur != null)
            {
                if(utilisateur.Mail.ToUpper() == loginForm.Mail.ToUpper())
                {
                    var token = Authenticator.GenerateToken(utilisateur);
                    return Ok(token);
                }
            }
            return NotFound("Email / Mot de passe incorrect.");
        }

        [Authorize]
        [HttpGet("current")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult current()
        {
            Authenticator.init(_config);
            JwtSecurityToken token = Authenticator.GetToken(HttpContext.Request.Headers["Authorization"]);
            string userId = token.Claims.First(x => x.Type == ClaimTypes.Email).Value;
            HttpContent content = Connect.Get("utilisateurs/bymail/" + userId);
            if (content != null)
            {
                return Ok(content.ReadFromJsonAsync<Utilisateur>().Result);
            }
            else
            {
                return NotFound("Utilisateur inconnu");
            }
        }

        [Authorize]
        [HttpGet("logout")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult Logout()
        {
            Response.Headers.Remove("Authorization");
            return Ok();
        }

    }
}
