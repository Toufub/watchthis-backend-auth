﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace AuthMicroService.Models
{
    public class Utilisateur
    {
        public Utilisateur()
        {
            UtilisateurId = -1;
            Nom = "";
            Prenom = "";
            Mail = "";
            ImageProfil = "";
            BetaSeriesId = "";
            DateCreation = new DateTime();
            NotificationActive = false;
        }
        public int UtilisateurId { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Mail { get; set; }
        public string? BetaSeriesId { get; set; }
        public string? ImageProfil { get; set; }
        public DateTime DateCreation { get; set; }
        public Boolean NotificationActive { get; set; }
    }
}
